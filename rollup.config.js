import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import babel from "rollup-plugin-babel";
import { eslint } from "rollup-plugin-eslint";
import cleanup from "rollup-plugin-cleanup";
import sizes from "rollup-plugin-sizes";
import json from "rollup-plugin-json";

export default {
  onwarn: function(warning, warn) {
    if (warning.code === "CIRCULAR_DEPENDENCY") return;
    warn(warning);
  },
  input: "src/index.js",
  output: {
    dir: "dist",
    format: "cjs"
  },
  watch: {
    chokidar: true,
    exclude: ["node_modules/**"]
  },
  external: [
    "react",
    "react-dom",
    "prop-types",
    "react-redux",
    "history",
    "redux-actions",
    "styled-components",
    "react-router",
    "redux",
    "redux-saga",
    "connected-react-router"
  ],
  plugins: [
    json({
      // All JSON files will be parsed by default,
      // but you can also specifically include/exclude files
      include: "src/**",
      exclude: ["node_modules/**"],

      // for tree-shaking, properties will be declared as
      // variables, using either `var` or `const`
      preferConst: true, // Default: false

      // specify indentation for the generated default export —
      // defaults to '\t'
      indent: "  ",

      // ignores indent and generates the smallest code
      compact: true, // Default: false

      // generate a named export for every property of the JSON object
      namedExports: true // Default: true
    }),
    resolve({ browser: true, module: true, jsnext: true }),
    eslint({
      extends: ["react-app", "airbnb", "prettier", "prettier/react"],
      plugins: ["react", "prettier", "import"],
      rules: {
        "react/no-array-index-key": 0,
        "no-underscore-dangle": 0,
        "react/require-default-props": 0,
        "react/prefer-stateless-function": 0,
        "react/prop-types": 0,
        "import/no-extraneous-dependencies": 0,
        "import/prefer-default-export": 0,
        "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx"] }],
        "linebreak-style": ["off"],
        "import/order": [
          "error",
          {
            "newlines-between": "always",
            groups: [
              ["builtin", "external", "internal"],
              ["parent", "index", "sibling"]
            ]
          }
        ]
      },
      parser: "babel-eslint"
    }),
    commonjs({
      // non-CommonJS modules will be ignored, but you can also
      // specifically include/exclude files
      include: ["node_modules/**", "../../node_modules/**"], // Default: undefined
      exclude: ["node_modules/foo/**", "node_modules/bar/**"], // Default: undefined
      // these values can also be regular expressions
      // include: /node_modules/

      // search for files other than .js files (must already
      // be transpiled by a previous plugin!)
      extensions: [".js", ".coffee"], // Default: [ '.js' ]

      // if true then uses of `global` won't be dealt with by this plugin
      ignoreGlobal: false, // Default: false

      // if false then skip sourceMap generation for CommonJS modules
      sourceMap: false, // Default: true

      // explicitly specify unresolvable named exports
      // (see below for more details)
      namedExports: { "./module.js": ["foo", "bar"] }, // Default: undefined

      // sometimes you have to leave require statements
      // unconverted. Pass an array containing the IDs
      // or a `id => boolean` function. Only use this
      // option if you know what you're doing!
      ignore: ["conditional-runtime-dependency"]
    }),
    babel({
      plugins: [
        "transform-class-properties",
        "@babel/plugin-syntax-dynamic-import"
      ],
      exclude: "node_modules/**" // only transpile our source code
    }),
    sizes(),
    cleanup()
  ]
};
