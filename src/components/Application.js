import React, { Component } from "react";
import { Provider } from "react-redux";

import CustomRouter from "./technical/customRouter/CustomRouter";
import Routes from "../routes/routes";

export default class Application extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <CustomRouter history={store.history}>
          <Routes />
        </CustomRouter>
      </Provider>
    );
  }
}
