import styled from "styled-components";
import {
  space,
  color,
  width,
  fontSize,
  flex,
  order,
  alignSelf,
  borders,
  position,
  zIndex,
  top,
  right,
  bottom,
  left,
  height,
  gridArea,
  gridColumn,
  gridRow,
  boxShadow,
  display,
  justifySelf
} from "styled-system";

import themed from "../helper";

export const Box = styled("div")(
  {
    boxSizing: "border-box"
  },
  space,
  color,
  width,
  fontSize,
  flex,
  order,
  alignSelf,
  borders,
  position,
  zIndex,
  top,
  right,
  bottom,
  left,
  height,
  gridArea,
  gridColumn,
  gridRow,
  boxShadow,
  display,
  justifySelf,
  props => props.css,
  themed("Box")
);

Box.displayName = "Box";

Box.propTypes = {
  ...space.propTypes,
  ...color.propTypes,
  ...width.propTypes,
  ...height.propTypes,
  ...borders.propTypes,
  ...fontSize.propTypes,
  ...gridArea.propTypes,
  ...boxShadow.propTypes,
  ...flex.propTypes,
  ...alignSelf.propTypes,
  ...justifySelf.propTypes,
  ...gridArea.propTypes,
  ...gridColumn.propTypes,
  ...gridRow.propTypes,
  ...order.propTypes,
  ...position.propTypes,
  ...zIndex.propTypes
};
