import styled from "styled-components";
import {
  flexWrap,
  flexDirection,
  alignItems,
  justifyContent,
  overflow,
  minHeight
} from "styled-system";

import { Box } from "../box/Box";
import themed from "../helper";

export const Flex = styled(Box)(
  {
    display: "flex"
  },
  flexWrap,
  flexDirection,
  alignItems,
  justifyContent,
  overflow,
  minHeight,
  themed("Flex")
);

Flex.displayName = "Flex";

Flex.propTypes = {
  ...flexWrap.propTypes,
  ...flexDirection.propTypes,
  ...alignItems.propTypes,
  ...justifyContent.propTypes,
  ...overflow.propTypes,
  ...minHeight.propTypes
};
