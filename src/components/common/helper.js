import { css } from "styled-components";
import { rgba } from "polished";

const themed = key => props => props.theme[key];

export const resolveColor = colorName => css`
  ${props => props.theme.colors[colorName]};
`;

export const resolveBackgroundColor = colorName => css`
  ${props => rgba(props.theme.colors[colorName], 0.8)};
`;

export const backgroundMixin = css`
  ${props =>
    (props.type === "info" && resolveBackgroundColor("mid-gray")) ||
    (props.type === "error" && resolveBackgroundColor("dark-red")) ||
    (props.type === "warning" && resolveBackgroundColor("yellow"))};
`;

export const colorMixin = css`
  ${props =>
    (props.type === "info" && resolveColor("mid-gray")) ||
    (props.type === "error" && resolveColor("dark-red")) ||
    (props.type === "warning" && resolveColor("yellow"))};
`;

export default themed;
