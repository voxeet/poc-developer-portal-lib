import { createBrowserHistory } from "history";

import { buildStore } from "./store/store";
import Application from "./components/Application";

export default function configure(oldConfiguration = {}) {
  let store;
  if (oldConfiguration.store) {
    store = oldConfiguration.store;
  } else {
    store = buildStore({
      history: createBrowserHistory()
    });
  }

  return {
    store: store,
    App: Application
  };
}
