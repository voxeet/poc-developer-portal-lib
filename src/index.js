import React from "react";

import configure from "./configureStore";

// TODO: to be fixed! 
const config = configure();
const App = config.App;

// interface to be defined! 
// export only main component? export helper functions? store, sagas, ...
export const Main = () => <App store={config.store}></App>;
