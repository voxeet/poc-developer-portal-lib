import React from "react";
import { Switch, Route } from "react-router";
import { ThemeProvider, createGlobalStyle } from "styled-components";

import { theme } from "../theme/theme";
import { Box } from "../components/common/box/Box";
import { Flex } from "../components/common/flex/Flex";

const GlobalStyle = createGlobalStyle`
  @font-face {
      font-family: "Gingham Variable";
      src: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/209981/Gingham.woff2")
              format("woff2"),
          url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/209981/Gingham.woff")
              format("woff");
  }
  body {    
    margin: 0px;
    width: 100vw;
    height: 100vh;
    background: #C5E1A5;
    color: #034908;    
    font-size: 26px;
    margin: 0;
    font-family: "Gingham Variable", BlinkMacSystemFont, sans-serif;
      font-variation-settings: "wght" 400, "wdth" 1000;
  }
`;

const routes = () => (
  <ThemeProvider theme={theme}>
    <Flex>
      <Route
        render={({ location }) => (
          <Switch location={location}>
            <Route path="/" render={() => <Box>authenticated</Box>} />
            <Route path="*" render={() => <Box>login</Box>} />
          </Switch>
        )}
      />
    </Flex>
    <GlobalStyle />
  </ThemeProvider>
);

export default routes;
