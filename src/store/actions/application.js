import { createEffect } from "../helper";

export const applicationStart = createEffect("APPLICATION_START");
export const applicationReady = createEffect("APPLICATION_READY");
