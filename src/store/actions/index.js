import * as user from "./user";
import * as application from "./application";
export default {
  user,
  application
};
