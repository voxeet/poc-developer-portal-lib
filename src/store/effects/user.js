import { createEffect } from "../helper";

export const login = createEffect("USER_LOGIN");
export const loginFail = createEffect("USER_LOGIN_FAIL");
export const logout = createEffect("USER_LOGOUT");
