import { login, logout, loginFail } from "../effects/user";
import { DEFECT_KEY } from "../defect";
import { handleEffects } from "../helper";

const notConnectedUser = { [DEFECT_KEY]: "Not connected" };

export default handleEffects(
  {
    [login]: (state, action) => ({ ...action.payload }),
    [logout]: () => notConnectedUser,
    [loginFail]: (state, action) => {
      if (action.payload.error === "Unauthorized")
        return { ...notConnectedUser, message: "Access not granted" };
      return { ...notConnectedUser, message: "Unexpected error" };
    }
  },
  notConnectedUser
);
