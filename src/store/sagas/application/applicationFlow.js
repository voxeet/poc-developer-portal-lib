import { put, delay } from "redux-saga/effects";

import actions from "../../actions";

export default function* applicationFlow() {
  // TODO:
  // 1- wait for something(or not) (start condition?),
  // 2- Get env variable,
  // 3- get current locale,
  // 4- get available locales and set locale,
  // 5- get and set translations,
  // 6- dispatch action/effect to store
  yield delay(3000); // wait for debugging purpose!
  yield put(actions.application.applicationReady());
}
