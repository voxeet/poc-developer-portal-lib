import { put } from "redux-saga/effects";

import authenticate from "../authenticate";
import effects from "../../../effects";

test("should authenticate user", () => {
  const user = { firstName: "john", lastName: "Doe" };
  const gen = authenticate();
  expect(gen.next(user).value).toEqual(put(effects.user.login(user)));
  expect(gen.next().done).toBeTruthy();
});
