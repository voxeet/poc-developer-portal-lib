import { put } from "redux-saga/effects";

import logout from "../logout";
import effects from "../../../effects";

test("should logout user", () => {
  const gen = logout();
  expect(gen.next().value).toEqual(put(effects.user.logout()));
  expect(gen.next().done).toBeTruthy();
});
