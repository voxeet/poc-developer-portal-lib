import { put } from "redux-saga/effects";

import effects from "../../effects";

export default function* authenticate() {
  yield put(
    effects.user.login({
      firstName: "john",
      lastName: "Doe"
    })
  );
}
