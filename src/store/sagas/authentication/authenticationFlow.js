import { call, takeEvery, take } from "redux-saga/effects";

import logout from "./logout";
import authenticate from "./authenticate";
import actions from "../../actions";

export default function* authenticationFlow() {
  yield take(actions.application.applicationReady);
  //TODO:
  // 1- get env variables form store,
  // 2- set authentication provider instance if neeeded,
  // 3- call authentication saga
  yield call(authenticate);
  yield takeEvery(actions.user.logout, logout);
}
