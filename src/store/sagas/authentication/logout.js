import { put } from "redux-saga/effects";

import effects from "../../effects";

export default function* logout() {
  yield put(effects.user.logout());
}
