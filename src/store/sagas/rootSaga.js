import { all } from "redux-saga/effects";

import applicationFlow from "./application/applicationFlow";
import authenticationFlow from "./authentication/authenticationFlow";

export default function* rootSaga() {
  yield all([applicationFlow(), authenticationFlow()]);
}
