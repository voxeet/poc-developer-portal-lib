import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import { connectRouter, routerMiddleware, push } from "connected-react-router";
import createSagaMidleware from "redux-saga";

import reducers from "./reducers/reducers";
import actions from "./actions";
import rootSaga from "./sagas/rootSaga";

const configureStore = (initialState, routerHistory) => {
  const router = routerMiddleware(routerHistory);
  const saga = createSagaMidleware();

  const staticReducers = {
    router: connectRouter(routerHistory),
    ...reducers
  };

  const createReducer = asyncReducers =>
    combineReducers({
      ...staticReducers,
      ...asyncReducers
    });

  const actionCreators = {
    ...actions,
    // todo check effect, missing here?
    push
  };

  const middlewares = [saga, router];

  const composeEnhancers = (() => {
    const compose_ = window && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    if (process.env.NODE_ENV === "development" && compose_) {
      return compose_({ actionCreators });
    }
    return compose;
  })();

  const enhancer = composeEnhancers(applyMiddleware(...middlewares));
  const store = createStore(createReducer(), initialState, enhancer);

  // Add a dictionary to keep track of the registered async reducers
  store.asyncReducers = {};

  // Create an inject reducer function
  // This function adds the async reducer, and creates a new combined reducer
  store.injectReducer = (key, asyncReducer) => {
    store.asyncReducers[key] = asyncReducer;
    store.replaceReducer(createReducer(this.asyncReducers));
  };

  saga.run(rootSaga);

  store.registerSagas = (...sagas) => {
    sagas.forEach(saga.run);
  };

  return store;
};

const syncHistoryWithStore = (store, history) => {
  const { router } = store.getState();
  if (router && router.location) {
    history.replace(router.location);
  }
};

export const buildStore = ({ initialState = {}, history }) => {
  const store = configureStore(initialState, history);
  syncHistoryWithStore(store, history);
  store.history = history;
  return store;
};
